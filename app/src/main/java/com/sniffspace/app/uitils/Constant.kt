package com.eisuchi.eisuchi.uitils


object Constant {


    // const val BASE_URL = "http://societyfy.in/lightmf/"
    // const val BASE_URL = "http://societyfy.in/lightmf_stagging/"

    private const val BASE_URL = "http://seorank.us/testing/eisuchi/site/"
    const val API_URL = "${BASE_URL}api/"

    const val TITLE = "title"
    const val TEXT = "text"
    const val QTY = "qty"
    const val INFO = "info"
    const val FORGPWD_URL="${BASE_URL}forgotpassword"

    // Common Params
    const val ACTION = "action"
    const val PARAMS = "param"
    const val ERROR = "error"
    const val DATA = "data"



    // ---Server Date Time--//
    const val DATE_FORMAT = "yyyy-MM-dd hh:mm:ss"



    const val ORDER_ID = "orderId"
    const val UNAUTHORIZED = "unauthorized"





    //--- Action Name-----

    const val LOGIN = "login"
    const val LOGOUT = "logout"
    const val ORDERS = "orders"
    const val ORDER = "order"
    const val DELIVERY = "deliver"
    const val QRCODES = "qrcodes"


}