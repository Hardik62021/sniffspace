package com.sniffspace.app.ui.view

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowInsets
import android.view.WindowManager
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.extention.goToActivityAndClearTask
import com.sniffspace.app.databinding.ActivitySplashBinding
import com.sniffspace.app.ui.home.HomeActivity


class SplashActivity : BaseActivity() {
    lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        Handler(Looper.getMainLooper()).postDelayed({
            validateRedirection()
        }, 1000)



       // binding.txtHello.text ="Got it"

    }
    private fun validateRedirection() {

        if (session.isLoggedIn) {
            //goToActivityAndClearTask<OrderListActivity>()
        } else{
            goToActivityAndClearTask<HomeActivity>()
        }
    }

}