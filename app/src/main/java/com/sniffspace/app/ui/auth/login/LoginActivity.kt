package com.sniffspace.app.ui.auth


import android.os.Bundle
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.extention.goToActivity
import com.sniffspace.app.databinding.ActivityLoginBinding
import com.sniffspace.app.ui.auth.forgot.ForgotPasswordActivity


class LoginActivity :BaseActivity() {

    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setClickListener()
    }

    // Click Event
    private fun setClickListener(){
        binding.txtForgotpwd.setOnClickListener { goToActivity<ForgotPasswordActivity>() }
    }
}