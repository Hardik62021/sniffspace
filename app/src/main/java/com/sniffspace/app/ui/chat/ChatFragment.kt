package com.sniffspace.app.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sniffspace.app.R
import com.sniffspace.app.databinding.FragmentChatNotLoginBinding
import com.sniffspace.app.databinding.FragmentProfileNotLoginBinding
import com.sniffspace.app.databinding.FragmentSearchMainBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.base.ViewPagerPagerAdapter

class ChatFragment :BaseFragment() {
    private var _binding: FragmentChatNotLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChatNotLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.include2.txtTitle.text ="Chat"

    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}