package com.sniffspace.app.ui.booking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sniffspace.app.R
import com.sniffspace.app.databinding.FragmentBookingNotLoginBinding
import com.sniffspace.app.databinding.FragmentProfileNotLoginBinding
import com.sniffspace.app.databinding.FragmentSearchMainBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.base.ViewPagerPagerAdapter

class BookingFragment :BaseFragment() {
    private var _binding: FragmentBookingNotLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBookingNotLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.include2.txtTitle.text ="Booking"

    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}