package com.sniffspace.app.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sniffspace.app.databinding.FragmentSearchListBinding
import com.sniffspace.app.databinding.FragmentSearchMainBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.base.ViewPagerPagerAdapter
import com.sniffspace.app.ui.home.HomePlayAdapter

class SearchFragmentList :BaseFragment(), SearchListAdapter.OnItemSelected {
    private var _binding: FragmentSearchListBinding? = null
    private val binding get() = _binding!!
    var list :MutableList<Int> = mutableListOf()
    var  adapter:SearchListAdapter?=null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dummyData()
        setupRecyclerView()

    }
    // setup Dummy List
    fun dummyData(){
        list.clear()
        for (i in 0 until 5){
            list.add(i)
        }
    }
    // Recyclerview Init
    fun setupRecyclerView() {
        val layoutManager = GridLayoutManager(mContext,2)
        //layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.include.recyclerView.layoutManager = layoutManager
        adapter = SearchListAdapter(requireContext(), list, session, "", this)
        binding.include.recyclerView.adapter = adapter

    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelect(position: Int, data: Int, action: String) {

    }
}