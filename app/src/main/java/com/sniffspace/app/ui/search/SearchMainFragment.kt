package com.sniffspace.app.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sniffspace.app.R
import com.sniffspace.app.databinding.FragmentSearchMainBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.base.ViewPagerPagerAdapter

class SearchMainFragment :BaseFragment() {
    private var _binding: FragmentSearchMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchMainBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStatePageAdapter()
    }

    private fun setStatePageAdapter() {
     val   viewPageradapter = ViewPagerPagerAdapter(childFragmentManager)
        // viewPageradapter.addFragment(CollectionChartFragment(), "Chart")
        viewPageradapter.addFragment(
           SearchFragmentList()
        )
        viewPageradapter.addFragment(
            SearchMapFragment()
        )

        _binding?.viewPager?.adapter = viewPageradapter
        _binding?.tabs?.setupWithViewPager(_binding?.viewPager!!, true)

        binding.tabs.getTabAt(0)?.setIcon(R.drawable.ic_list_green)
        binding.tabs.getTabAt(1)?.setIcon(R.drawable.ic_location_grey)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}