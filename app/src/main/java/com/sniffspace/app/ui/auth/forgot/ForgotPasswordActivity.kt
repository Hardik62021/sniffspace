package com.sniffspace.app.ui.auth.forgot

import android.os.Bundle
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.sniffspace.app.databinding.ActivityForgotPwdBinding

class ForgotPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityForgotPwdBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPwdBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.include.txtTitle.text = "Forgot"
    }
}