package com.sniffspace.app.ui.home


import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eisuchi.utils.SessionManager
import com.sniffspace.app.R
import com.sniffspace.app.databinding.RowHomeGoodReadBinding
import com.sniffspace.app.databinding.RowHowItWorksBinding


class HomeHowWorkAdapter(
    private val mContext: Context,
    var list: MutableList<Int> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: HomeHowWorkAdapter.OnItemSelected,
) : RecyclerView.Adapter<HomeHowWorkAdapter.ItemHolder>() {

    lateinit var binding: RowHowItWorksBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowHowItWorksBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: Int, action: String)
    }

    class ItemHolder(containerView: RowHowItWorksBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: Int,
            listener: OnItemSelected, session: SessionManager
        ) {
            val word = SpannableString("Step ")

            word.setSpan(
                ForegroundColorSpan(Color.BLACK),
                0,
                word.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            binding.texStep.text = word
            val wordTwo: Spannable = SpannableString("0"+data.toString())

            wordTwo.setSpan(
                ForegroundColorSpan(Color.parseColor("#56A927")),
                0,
                wordTwo.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            binding.texStep.append(wordTwo)

          //  Glide.with(context).load(data.imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(binding.imageView2)
          //  Glide.with(context).load(R.drawable.test).into(binding.img)


          //  binding.btnDetail.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}