package com.sniffspace.app.ui.auth


import android.os.Bundle
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.extention.goToActivity
import com.eisuchi.extention.goToActivityAndClearTask
import com.eisuchi.extention.hide
import com.eisuchi.extention.invisible
import com.sniffspace.app.R
import com.sniffspace.app.databinding.ActivityLoginBinding
import com.sniffspace.app.databinding.ActivityVerificationMsgBinding
import com.sniffspace.app.ui.auth.forgot.ForgotPasswordActivity
import com.sniffspace.app.ui.home.HomeActivity


class AccountVerificationActivity :BaseActivity() {

    lateinit var binding: ActivityVerificationMsgBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerificationMsgBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setClickListener()
        binding.include.imgBack.setImageResource(R.drawable.ic_close)
        binding.include.txtTitle.hide()
    }

    // Click Event
    private fun setClickListener(){
        binding.btnHome.setOnClickListener { goToActivityAndClearTask<HomeActivity>() }
    }
}