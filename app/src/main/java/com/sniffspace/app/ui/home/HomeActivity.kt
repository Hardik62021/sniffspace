package com.sniffspace.app.ui.home


import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.extention.replaceFragment
import com.eisuchi.extention.showToast
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sniffspace.app.R
import com.sniffspace.app.customview.BottomNavigationSquare
import com.sniffspace.app.databinding.ActivityHomeBinding
import com.sniffspace.app.ui.booking.BookingFragment
import com.sniffspace.app.ui.chat.ChatFragment
import com.sniffspace.app.ui.profile.ProfileFragment


class HomeActivity : BaseActivity(){
    lateinit var binding: ActivityHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val bottomNavigation = findViewById<BottomNavigationSquare>(R.id.bottomNavigation)
        bottomNavigation.textColor = Color.parseColor("#56A927")


       bottomNavigation.setOnItemSelectedListener {
           when (it.itemId) {
               R.id.action_home -> {
                   replaceFragment(HomeFragment(), R.id.framMain)
                   return@setOnItemSelectedListener true
               }
               R.id.action_booking -> {
                   replaceFragment(BookingFragment(), R.id.framMain)
                   return@setOnItemSelectedListener true
               }
               R.id.action_message -> {
                   replaceFragment(ChatFragment(), R.id.framMain)
                   return@setOnItemSelectedListener true
               }
               R.id.action_profile -> {
                   replaceFragment(ProfileFragment(), R.id.framMain)
                   return@setOnItemSelectedListener true
               }
           }
           false
       }

        replaceFragment(HomeFragment(), R.id.framMain)
    }




}