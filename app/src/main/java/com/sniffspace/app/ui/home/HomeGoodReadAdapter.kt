package com.sniffspace.app.ui.home


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eisuchi.utils.SessionManager
import com.sniffspace.app.R
import com.sniffspace.app.databinding.RowHomeGoodReadBinding


class HomeGoodReadAdapter(
    private val mContext: Context,
    var list: MutableList<Int> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: HomeGoodReadAdapter.OnItemSelected,
) : RecyclerView.Adapter<HomeGoodReadAdapter.ItemHolder>() {

    lateinit var binding: RowHomeGoodReadBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowHomeGoodReadBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: Int, action: String)
    }

    class ItemHolder(containerView: RowHomeGoodReadBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: Int,
            listener: OnItemSelected, session: SessionManager
        ) {
            if (data%2==0)
                Glide.with(context).load(R.drawable.test2).into(binding.img)
            else
                Glide.with(context).load(R.drawable.test5).into(binding.img)
          //  Glide.with(context).load(data.imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(binding.imageView2)
          //  Glide.with(context).load(R.drawable.test).into(binding.img)


          //  binding.btnDetail.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}