package com.sniffspace.app.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eisuchi.extention.goToActivity
import com.sniffspace.app.databinding.FragmentProfileNotLoginBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.auth.LoginActivity
import com.sniffspace.app.ui.auth.signup.SignupActivity

class ProfileFragment :BaseFragment() {
    private var _binding: FragmentProfileNotLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileNotLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.include2.txtTitle.text ="Profile"
        setClickListener()

    }


    // Click Event
    fun setClickListener(){
        binding.btnLogin.setOnClickListener { goToActivity<LoginActivity>() }
        binding.btnSignup.setOnClickListener { goToActivity<SignupActivity>() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}