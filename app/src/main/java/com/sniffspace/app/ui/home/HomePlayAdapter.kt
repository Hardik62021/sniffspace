package com.sniffspace.app.ui.home


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eisuchi.utils.SessionManager
import com.sniffspace.app.R
import com.sniffspace.app.databinding.RowHomePlayBinding


class HomePlayAdapter(
    private val mContext: Context,
    var list: MutableList<Int> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: HomePlayAdapter.OnItemSelected,
) : RecyclerView.Adapter<HomePlayAdapter.ItemHolder>() {

    lateinit var binding: RowHomePlayBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowHomePlayBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: Int, action: String)
    }

    class ItemHolder(containerView: RowHomePlayBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: Int,
            listener: OnItemSelected, session: SessionManager
        ) {

          //  Glide.with(context).load(data.imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(binding.imageView2)
           if (data%2==0)
            Glide.with(context).load(R.drawable.test3).into(binding.img)
           else
               Glide.with(context).load(R.drawable.test4).into(binding.img)

          //  binding.btnDetail.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}