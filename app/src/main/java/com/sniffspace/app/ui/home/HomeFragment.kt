package com.sniffspace.app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eisuchi.extention.replaceFragment
import com.sniffspace.app.R
import com.sniffspace.app.databinding.FragmentHomeBinding
import com.sniffspace.app.databinding.FragmentSearchListBinding
import com.sniffspace.app.ui.base.BaseFragment
import com.sniffspace.app.ui.search.SearchMainFragment

class HomeFragment :BaseFragment() ,HomePlayAdapter.OnItemSelected, HomeGoodReadAdapter.OnItemSelected, HomeHowWorkAdapter.OnItemSelected , HomeLinksAdapter.OnItemSelected{
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    var adapterPlay:HomePlayAdapter?=null
    var adapterGoodRead:HomeGoodReadAdapter?=null
    var adapterHowWork:HomeHowWorkAdapter?=null
    var adapterLinks:HomeLinksAdapter?=null
    var list :MutableList<Int> = mutableListOf()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dummyData()
        setupRecyclerView()
        setupRecyclerView2()
        setupRecyclerView3()
        setupRecyclerView4()

        binding.edtSearch.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> replaceFragment(SearchMainFragment(), R.id.framMain)
            }
            v?.onTouchEvent(event) ?: true
        }

    }

    // Recyclerview Init
    fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(mContext)
        layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.rvPlay.layoutManager = layoutManager
        adapterPlay = HomePlayAdapter(requireContext(), list, session, "", this)
        binding.rvPlay.adapter = adapterPlay

    }
    // Recyclerview Init
    fun setupRecyclerView2() {
        val layoutManager = LinearLayoutManager(mContext)
        layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.rvRead.layoutManager = layoutManager
        adapterGoodRead = HomeGoodReadAdapter(requireContext(), list, session, "", this)
        binding.rvRead.adapter = adapterGoodRead

    }

    // Recyclerview Init
    fun setupRecyclerView3() {
        val layoutManager = LinearLayoutManager(mContext)
        // layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.rvHowItWorks.layoutManager = layoutManager
        adapterHowWork = HomeHowWorkAdapter(requireContext(), list, session, "", this)
        binding.rvHowItWorks.adapter = adapterHowWork

    }

    // Recyclerview Init
    fun setupRecyclerView4() {
        val layoutManager = LinearLayoutManager(mContext)
        // layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.rvLinks.layoutManager = layoutManager
        adapterLinks = HomeLinksAdapter(requireContext(), list, session, "", this)
        binding.rvLinks.adapter = adapterLinks

    }

    override fun onItemSelect(position: Int, data: Int, action: String) {

    }

    // setup Dummy List
    fun dummyData(){
        list.clear()
        for (i in 0 until 5){
            list.add(i)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}