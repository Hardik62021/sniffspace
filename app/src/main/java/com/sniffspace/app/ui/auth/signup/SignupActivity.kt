package com.sniffspace.app.ui.auth.signup


import android.os.Bundle
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.extention.goToActivity
import com.sniffspace.app.databinding.ActivityLoginBinding
import com.sniffspace.app.databinding.ActivitySignupBinding
import com.sniffspace.app.ui.auth.forgot.ForgotPasswordActivity


class SignupActivity :BaseActivity() {

    lateinit var binding: ActivitySignupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setClickListener()
        binding.include3.txtTitle.text ="Sign Up"
    }

    // Click Event
    private fun setClickListener(){
       // binding.txtForgotpwd.setOnClickListener { goToActivity<ForgotPasswordActivity>() }
    }
}