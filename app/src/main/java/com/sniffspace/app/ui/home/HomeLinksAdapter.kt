package com.sniffspace.app.ui.home


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eisuchi.utils.SessionManager
import com.sniffspace.app.R
import com.sniffspace.app.databinding.RowHomePlayBinding
import com.sniffspace.app.databinding.RowLinksBinding


class HomeLinksAdapter(
    private val mContext: Context,
    var list: MutableList<Int> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: HomeLinksAdapter.OnItemSelected,
) : RecyclerView.Adapter<HomeLinksAdapter.ItemHolder>() {

    lateinit var binding: RowLinksBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowLinksBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: Int, action: String)
    }

    class ItemHolder(containerView: RowLinksBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: Int,
            listener: OnItemSelected, session: SessionManager
        ) {

          //  Glide.with(context).load(data.imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(binding.imageView2)


          //  binding.btnDetail.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}